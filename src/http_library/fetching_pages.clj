(ns http-library.fetching-pages
  (:require [clj-http.client :as client])
  (:require [clojure.java.io :as io]))

(client/head "http://something.com/")

(get (get (client/head "http://something.com/") :headers) "Content-Type")

(client/get "http://something.com/")

(:body (client/get "http://something.com/"))

(client/head "https://mindaslab.github.io/me.jpg")

;; doesn't work, not sure what it is
(spit "me.jpg" (:body (client/get "https://mindaslab.github.io/me.jpg")))

(client/get "https://mindaslab.github.io/me.jpg")

(defn copy [uri file]
  (with-open [in (io/input-stream uri)
              out (io/output-stream file)]
    (io/copy in out)))

(copy "https://mindaslab.github.io/me.jpg" "paparapine.jpg")

;; doesn't work, not sure what it is
(spit "spit_picture.jpg" (slurp "https://mindaslab.github.io/me.jpg"))
